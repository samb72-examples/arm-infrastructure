param (
    # Enable Verbose Logging
    [switch] $VerboseLogging
)


. .\AzureAutomationFunctions.ps1



if($VerboseLogging) { $VerbosePreference = 'Continue' }
else { $VerbosePreference = 'SilentlyContinue'}


$checkExtensionScriptBlock = {
    . .\AzureAutomationFunctions.ps1
    $extensions = Get-AzureWebAppSiteExtensions -WebApp $Using:webApp
    $appInsightsExtension = $extensions | Where-Object { $_.id -eq 'Microsoft.ApplicationInsights.AzureWebSites' }

    # If appInsights is installed, tag the webApp as connected. Otherwise tag as not connected.
    $appInsightsConnected = $appInsightsExtension -ne $null
    $Using:webApp | Set-AzureRmResource -Tag { AppInsightsConnected = $appInsightsConnected}
}


# Connect to azure if not currently connected
Connect-AzureAccount

$subscriptionNames = (Get-AzureRmSubscription).Name

foreach($sub in $subscriptionNames) {
    Write-Output "Switching to subscription: $sub."
    try {
        $null = Select-AzureRmSubscription -SubscriptionName $sub

        $webApps = Get-AzureRmWebApp
        $webAppJobs = @()

        foreach($webApp in $webApps)
        {
            Write-Output "Tagging WebApp: $($webApp.Name)"
            $webAppJobs += Start-Job -ScriptBlock {
                # 'Using' variables. We retrieve the webapp object again so its not deserialized in the job.
                $path = ($Using:pwd).Path
                $webAppId = ($Using:webApp).Id
                $webApp = Get-AzureRmResource -ResourceId $webAppId | Get-AzureRmWebApp

                Write-Host $webAppId
                # Set path to local path to import functions
                $null = Set-Location -Path $path
                . .\AzureAutomationFunctions.ps1

                # Check if Application Insights extension is installed
                $extensions = Get-AzureWebAppSiteExtensions -WebApp $webApp
                $appInsightsExtension = $extensions | Where-Object { $_.id -eq 'Microsoft.ApplicationInsights.AzureWebSites' }

                # If appInsights is installed, tag the webApp as connected. Otherwise tag as not connected.
                $appInsightsConnected = $null -ne $appInsightsExtension
                $null = Set-AzureRmResource -Tag @{ AppInsightsConnected = $appInsightsConnected } -ResourceId $webAppId -Force
            }
        }

        Wait-JobComplete -Job $webAppJobs
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
    finally {
        Get-Job | Receive-Job
        Get-Job | Remove-Job -Force
    }

}
