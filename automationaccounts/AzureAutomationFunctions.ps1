Import-Module AzureRM -Force


<#
.SYNOPSIS
    Function to check for current azure connection and if none exists then attempt to connect.
.DESCRIPTION
    Function to check for current azure connection and if none exists then attempt to connect.
.EXAMPLE
    Connect-AzureAccount
#>

function Connect-AzureAccount {
    [CmdletBinding()]
    
    $connectionName = "AzureRunAsConnection"
    try {
        # Get the connection "AzureRunAsConnection "
        
        $currentContext = Get-AzureRmContext
        if($currentContext)
        {
            Write-Output "Connection to Azure already exists."
            return
        }

        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName         

        Write-Output "Logging in to Azure..."
        Add-AzureRmAccount `
            -ServicePrincipal `
            -TenantId $servicePrincipalConnection.TenantId `
            -ApplicationId $servicePrincipalConnection.ApplicationId `
            -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
    }
    catch {
        if (!$servicePrincipalConnection) {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }
}


<#
.SYNOPSIS
    Retrieve the SCM credentials for a WebApp
.DESCRIPTION
    Retrieve the SCM credentials for a WebApp
.EXAMPLE
   Get-AzureWebAppSCMCredentials -WebApp $webApp
.INPUTS
    WebApp - A WebApp object to retrieve site extensions for.
.OUTPUTS
    An object containing the SCM site, username and password.
#>

function Get-AzureWebAppSCMCredentials {

    [CmdletBinding()]
    
    param (
        # Web App Object to retrieve site extensions for
        [Parameter(Mandatory)]
        [Microsoft.Azure.Management.WebSites.Models.Site]
        $WebApp
    )
    
    # Get the webapp publishing profile in order to get scm credentials
    try {
        [xml]$publishingProfile = $WebApp | Get-AzureRmWebAppPublishingProfile
        $webProfile = $publishingProfile.publishData.publishProfile | Where-Object { $_.publishMethod -eq 'MSDeploy' }
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception
        return
    }

    $scmCreds = New-Object -TypeName psobject
    $scmCreds | Add-Member -MemberType NoteProperty -Name appUrl -Value "https://$($webProfile.publishUrl)"
    $scmCreds | Add-Member -MemberType NoteProperty -Name userName -Value $webProfile.userName
    $scmCreds | Add-Member -MemberType NoteProperty -Name password -Value $webProfile.userPWD

    return $scmCreds

}


<#
.SYNOPSIS
    A function to retrieve installed site extensions for a Web App
.DESCRIPTION
    A function to retrieve installed site extensions for a Web App
.EXAMPLE
    Get-AzureWebAppSiteExtensions -WebApp $webApp
.INPUTS
    WebApp - A WebApp object to retrieve site extensions for.
.OUTPUTS
    An array of site extensions.
#>

function Get-AzureWebAppSiteExtensions {

    [CmdletBinding()]
    
    param (
        # Web App Object to retrieve site extensions for
        [Parameter(Mandatory)]
        #[Microsoft.Azure.Management.WebSites.Models.Site]
        [psobject]
        $WebApp
    )
    
    $scmCreds = Get-AzureWebAppSCMCredentials -WebApp $WebApp

    # Use SCM api to retrieve all installed extensions
    $extensionUrl = "$($scmCreds.appUrl)/api/siteextensions"
    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $scmCreds.userName,$scmCreds.password)))
    $extensionData = Invoke-RestMethod -Method Get -Uri $extensionUrl -Headers @{Authorization=("Basic $base64AuthInfo")}

    return $extensionData
}

function Wait-JobComplete {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [psobject[]]
        $Job,

        # Timeout in seconds for jobs to complete
        [Parameter()]
        [int]
        $Timeout = 120,

        # Interval to wait to check on job status
        [Parameter()]
        [int]
        $Interval = 5
    )

    $jobsRunning = $Job | Get-Job | Where-Object { $_.State -eq 'Running' }
    $loopCount = 0

    while($jobsRunning)
    {
        if(($loopCount * $Interval) -gt $Timeout) {
            Write-Error "Timeout waiting for jobs to complete."
            throw "Timeout waiting for jobs to complete."
            return
        }
        $jobCount = $jobsRunning.count
        Write-Verbose "$jobCount jobs still running. Checking in $interval seconds."
        Start-Sleep -Seconds $Interval
        $jobsRunning = $Job | Get-Job | Where-Object { $_.State -eq 'Running' }
    }
    
    Write-Verbose "All jobs complete."

}